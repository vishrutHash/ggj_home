﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EZCameraShake;

public class BulletController : MonoBehaviour {

	// Use this for initialization
	void OnCollisionEnter(Collision col)
	{
		if(col.gameObject.CompareTag("WallTag"))
		{
			Debug.LogError("Shaking the Camera!");
			ShakeCamera();
			Destroy(this.gameObject);
		}
	}

	void ShakeCamera()
	{
		Debug.LogError("Camera is Shook!");
		CameraShaker.Instance.ShakeOnce(4f, 4f, .1f, .1f);
	}
}
