﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum PlayerType
	{
		FIGHTING_PLAYER,
		SHOOTING_PLAYER
	}

	public enum PowerUpType
	{
		GUN,
		WAVE_KILLER, // LOW PRIORITY
		EXPLOSION // LOW PRIORITY
	}
	public enum PlayerState
	{
		FIGHTING_STATE,
		SHOOTING_STATE
	}
public class GameController : MonoBehaviour {


	#region PLAYERS -------------------------------
	public GameObject shootingPLayer;
	public GameObject fightingPlayer;
	#endregion

	public List<Transform> gunSpawnPoints ;
	public GameObject gunLifeObject;
	GameObject gunLifeRef;
	public PlayerState playerState;
	float gameTimer = 0.0f;
	bool isGunPowerUpEnabled = false;
	private float shootingPlayerAvailableTime = 20f;

	private float powerUpAvailableTime = 5;
	
	// Use this for initialization
	void Start () {
		playerState = PlayerState.FIGHTING_STATE;
	 	TogglePlayer(false);
	}
	
	// Update is called once per frame
	void Update () {
		gameTimer +=Time.deltaTime;
		Debug.Log(gameTimer);
		if(gameTimer > 10.5f)
		{
			if(!isGunPowerUpEnabled)
			{
				HandleGunPowerUp();
			}
			gameTimer = 0;
		}

	}

	void HandleGunPowerUp()
	{
		isGunPowerUpEnabled = true;
		int index = Random.Range(0,gunSpawnPoints.Count);
		gunLifeRef = Instantiate(gunLifeObject,gunSpawnPoints[index].position,Quaternion.identity);
		StartCoroutine("GunPowerAvailableTime");
	}

	IEnumerator GunPowerAvailableTime()
	{
		yield return new WaitForSeconds(powerUpAvailableTime);
		Destroy(gunLifeRef);
		if(playerState != PlayerState.SHOOTING_STATE)
			isGunPowerUpEnabled = false;
	}

	public void TogglePlayer(bool status)// Status is TRUE for Activating ShootingPlayer and False for Deactivating ShootingPLayer
	{
		shootingPLayer.SetActive(status);
		fightingPlayer.SetActive(!status);
	}

	public void DestroyPowerUpReferences(PowerUpType powerUpType)
	{
		switch(powerUpType)
		{
			case PowerUpType.GUN : 
			Destroy(gunLifeRef);
			TogglePlayer(true);
			playerState = PlayerState.SHOOTING_STATE;
			StartCoroutine("GunPlayerMechanim");
			break;
		}
	}

	IEnumerator GunPlayerMechanim()
	{
		yield return new WaitForSeconds(shootingPlayerAvailableTime);
		TogglePlayer(false);
		isGunPowerUpEnabled = false;
	}
}
