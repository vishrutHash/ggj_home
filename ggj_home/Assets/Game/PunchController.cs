﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PunchController : MonoBehaviour {

	void OnCollisionEnter(Collision col)
	{
		if(col.gameObject.CompareTag("ENEMIES"))
			transform.root.GetComponent<PlayerFightController>().OnHittingEnemy();
	}
}
