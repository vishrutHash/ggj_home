﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFightController : MonoBehaviour {
	private PlayerType playerType;
	public GameObject bullets;

	[SerializeField]
	private Transform gun;
	[Range(20,200)]
	public float shootForce=20.5f;

	#region CAMERA SHAKING----------------------------------
	
	float shakeMagnitude = 1.5f;
	float shakeDuration = 2;
	private Camera cameraRef;
	private Vector3 cameraDefaultPosition;

	bool isPunching = false;
	#endregion
	void Start()
	{
		cameraRef = GetComponent<PlayerMovementController>().cameraRef;
		cameraDefaultPosition = cameraRef.transform.position;
		playerType = GetComponent<PlayerMovementController>().playerType;
	}
	void Update()
	{
		if(Input.GetMouseButtonDown(0))
		{
			if(playerType == PlayerType.SHOOTING_PLAYER)
			{
				GameObject bulletRef = Instantiate(bullets,gun.transform.position+gun.forward,Quaternion.identity);
				bulletRef.GetComponent<Rigidbody>().AddForce(gun.forward * shootForce,ForceMode.Impulse);
				SoundManager.Instance.OnBulletShoot();
				StartCoroutine("ShakeCamera");
			}
			else
			{
				// He is a fighting player
				Debug.LogError("Player is hitting");
				GetComponent<PlayerMovementController>().animator.SetBool("isFighting",true);
				isPunching = true;

			}
		}
		if(Input.GetMouseButtonUp(0))
		{
			if(playerType != PlayerType.SHOOTING_PLAYER)
				GetComponent<PlayerMovementController>().animator.SetBool("isFighting",false);
				isPunching = false;
		}
	}

	public void OnHittingEnemy()
	{
		Debug.LogError("Enemey punched");
	}

	IEnumerator ShakeCamera()
	{
		Debug.LogError("IN shake co-routine!");
		while(shakeDuration >0)
		{
			cameraRef.transform.position = new Vector3(cameraRef.transform.position.x + Random.Range(-1,1),
			cameraRef.transform.position.y + Random.Range(-1,1),cameraRef.transform.position.z) * shakeMagnitude;
			shakeDuration -= Time.deltaTime;
		}
		shakeDuration = 2;
		cameraRef.transform.position = cameraDefaultPosition;
		yield return null;
	}
}
