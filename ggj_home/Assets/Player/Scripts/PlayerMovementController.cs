﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementController : MonoBehaviour {
	[SerializeField]
	private GameController gameController;
	public PlayerType playerType;
	Vector3 playerVector;
	float speed = 2.5f;

	public Camera cameraRef;
	[HideInInspector]
	public Animator animator;

	Rigidbody rigidBody;
	Vector3 myMousePosition;

	// Use this for initialization
	void Start () {

		rigidBody = this.GetComponent<Rigidbody>();
		animator = transform.GetChild(0).GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {

		 if (
                  Input.GetKeyUp  (KeyCode.D) || Input.GetKeyUp (KeyCode.RightArrow) || 
                  Input.GetKeyUp (KeyCode.A) || Input.GetKeyUp (KeyCode.LeftArrow) || 
                  Input.GetKeyUp (KeyCode.W) || Input.GetKeyUp (KeyCode.UpArrow) || 
                  Input.GetKeyUp (KeyCode.S) || Input.GetKeyUp (KeyCode.DownArrow)            
              ) {
  
                  Stop ();
              }


		myMousePosition = cameraRef.ScreenToWorldPoint( new Vector3(
			Input.mousePosition.x,Input.mousePosition.y,cameraRef.transform.position.y));

		transform.LookAt(myMousePosition + Vector3.up * transform.position.y);

		playerVector = new Vector3(Input.GetAxis("Horizontal"),0,Input.GetAxis("Vertical")).normalized;
		Debug.LogError("Playervector="+playerVector);
	}

	void FixedUpdate()
	{
		if(playerVector.magnitude !=0)
		{
			rigidBody.MovePosition(rigidBody.position + playerVector * Time.fixedDeltaTime * speed);
			animator.SetBool("isMoving",true);
		}
		else
		{
			animator.SetBool("isMoving",false);
		}
		 
	}

	void Stop()
	{
		rigidBody.velocity = Vector3.zero;
	}

}
