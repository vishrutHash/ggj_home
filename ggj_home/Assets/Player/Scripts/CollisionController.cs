﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionController : MonoBehaviour {

	void OnCollisionEnter(Collision col)
	{
		if(col.gameObject.CompareTag("GunPowerUpTag"))
		{
			FindObjectOfType<GameController>().DestroyPowerUpReferences(PowerUpType.GUN);
		}
	}
}
