﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

	public List<AudioClip> sfxAudioClips;
	private AudioSource sfxAudioSource;

	public static SoundManager Instance;

	void Awake()
	{
		Instance = this;
		DontDestroyOnLoad(gameObject);
	}

	void Start()
	{
		sfxAudioSource = GetComponent<AudioSource>();
	}

	public void OnBulletShoot()
	{
		sfxAudioSource.clip = sfxAudioClips[0];
		sfxAudioSource.Play();
	}
}
